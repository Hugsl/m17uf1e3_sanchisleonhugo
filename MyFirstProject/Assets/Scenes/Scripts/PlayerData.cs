using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour
{
    public string PlayerName;
    public float Height=1.50f;
    public int[] stats;
    
    public float size;
    public float maxsize;
    public float minsize;
    public float sizetime;

    
    public float weight;
    public float speed;
    public bool control;
    public bool direction;
    public bool high;


    private bool moving;
    enum state
    {
        small,
        grow,
        big,
        degrow,
        cooldown
    };
    state estado;

    [SerializeField]
    private int _yearsOld = 20;
    [SerializeField]
    private Sprite _sprite;

    private int _frameIndex = 0;
    private float _count;

    [SerializeField]
    private float _frameRate;


    [SerializeField]
    private Sprite[] _sprites;

    // Start is called before the first frame update
    void Start()
    {
        moving = false;
        sizetime = 0;
        maxsize = 10f;
        minsize = 5f;
        estado =state.small;
        control = false;
        _count = 0;
        _frameRate = 12*Time.deltaTime;
        size = 1f;
        speed = 1f*Time.deltaTime;
        weight = 0;
        direction = false;
        high=false;

       // Debug.Log(this.gameObject.GetComponent<SpriteRenderer>().sprite.name);
   
    }

    // Update is called once per frame
    void Update()
    {
        
        checkSize();
        if (control == false)
        { moving = true; checkDir(); movement(); }
        else {
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                moving = true;
                transform.localRotation = Quaternion.Euler(0, 0, 0); }

            else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow)) { 
                moving=true;
                transform.localRotation = Quaternion.Euler(0, 180, 0); 

            }
            else { moving = false; }
            
            transform.position = new Vector3((Input.GetAxis("Horizontal") * speed) + transform.position.x, (Input.GetAxis("Vertical") * speed) + transform.position.y, transform.position.z);

        }

        //if (moving) { }  else{ }
        if (_count > _frameRate)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = _sprites[_frameIndex];
            _frameIndex = (_frameIndex < _sprites.Length - 1) ? _frameIndex + 1 : 0;
            _count = 1*Time.deltaTime;

        }
        else
        {
            _count += 1*Time.deltaTime;
        }

        if (Input.GetKeyDown("space")) control = !control;

        
    }
    void movement()
    {
        

        if (direction) {
            if (transform.position.y > 2) high = !high;
            else if (transform.position.y < -2) high=!high;
            
            if (high) transform.position = new Vector3((speed) + transform.position.x, (-speed) + transform.position.y, transform.position.z);

            else if (!high) { transform.position = new Vector3(speed + transform.position.x, speed + transform.position.y, transform.position.z);  
                   


        }

    }


        else if(!direction)
        {
            if (transform.position.y > 2) high = !high;
            else if (transform.position.y < -2) high = !high;
            if (high) transform.position = new Vector3((-speed) + transform.position.x, (-speed ) + transform.position.y, transform.position.z);
            else if (!high) transform.position = new Vector3(-speed + transform.position.x, speed  + transform.position.y, transform.position.z);
                    


            
        }
    }

    void checkDir()
    {
        if (transform.position.x > 7)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            direction = !direction; }
        else if (transform.position.x < -7){ direction = !direction; transform.localRotation = Quaternion.Euler(0, 180, 0); }
    }

    void checkSize()
    {
        switch (estado)
        {
            case state.small:
                if (Input.GetKeyDown("b")) estado = state.grow;
                break;

            case state.grow:
                if (this.transform.localScale.x < maxsize) { 
                    transform.localScale = new Vector2(transform.localScale.x + (1f * Time.deltaTime), transform.localScale.y + (1f * Time.deltaTime));
                    speed -= 0.01f * Time.deltaTime;
                    _frameRate += 0.01f * Time.deltaTime;
                } 
                else estado = state.big;
                break;

            case state.big:
                if (sizetime < 10) sizetime += 1 * Time.deltaTime;
                else
                {
                    sizetime = 0;
                    estado = state.degrow;
                }
                break;

            case state.degrow:
                if (this.transform.localScale.x > minsize) {
                    transform.localScale = new Vector2(transform.localScale.x - (1f * Time.deltaTime), transform.localScale.y - (1f * Time.deltaTime));
                    _frameRate -= 0.01f * Time.deltaTime;
                    speed += 0.01f * Time.deltaTime; 
                }
              
                else estado = state.cooldown;
                break;

            case state.cooldown:
                if (sizetime < 10)
                {
                    sizetime += 1 * Time.deltaTime;
                    if (Input.GetKeyDown("b")) Debug.Log("Hability is on cool down");
                }
                else
                {
                    sizetime = 0;
                    estado = state.small;
                }
                break;
        }
    }
}
